import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedMaterialModule } from '@shared-material/shared-material.module';
import { UserCardComponent } from './components/user-card/user-card.component';
import { UsersListItemComponent } from './components/users-list-item/users-list-item.component';


@NgModule({
  declarations: [UserCardComponent, UsersListItemComponent],
  imports: [
    CommonModule, SharedMaterialModule
  ],
  exports: [UserCardComponent, UsersListItemComponent]
})
export class SharedModule {
}
