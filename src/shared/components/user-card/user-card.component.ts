import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { IUser } from '../../../interfaces/i-user';
import { UsersService } from '@users-dashboard/services/users.service';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserCardComponent implements OnInit {

  user$: Observable<IUser>;

  constructor(private userService: UsersService) {
  }

  ngOnInit(): void {
    this.user$ = this.userService.selectedUser$.pipe(switchMap((userId: string) => this.userService.getUserById(userId)));
  }

}
