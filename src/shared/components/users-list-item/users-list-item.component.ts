import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { IUser } from '../../../interfaces/i-user';

@Component({
  selector: 'app-users-list-item',
  templateUrl: './users-list-item.component.html',
  styleUrls: ['./users-list-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UsersListItemComponent {

  @Input() user: IUser;
  @Input() selected: boolean;
  @Output() clicked = new EventEmitter<string>();

  onClick(): void {
    this.clicked.emit(this.user.id);
  }

}
