import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersDashboardModule } from '@users-dashboard/users-dashboard.module';
import { environment } from '../environments/environment';
import { ENVIRONMENT } from './services/environment.service';
import { MainLayoutComponent } from './components/main-layout/main-layout.component';



@NgModule({
  declarations: [MainLayoutComponent],
  imports: [
    CommonModule, UsersDashboardModule
  ],
  providers: [
    { provide: ENVIRONMENT, useValue: environment },
  ], exports: [MainLayoutComponent]
})
export class CoreModule { }
