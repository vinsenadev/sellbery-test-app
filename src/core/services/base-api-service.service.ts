import { Observable } from 'rxjs';
import { IUser } from '../../interfaces/i-user';


export abstract class BaseApiServiceService {
  abstract search(searchStr: string): Observable<IUser[]>;

}
