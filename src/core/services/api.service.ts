import { Injectable } from '@angular/core';
import { BaseApiServiceService } from '@core/services/base-api-service.service';
import { Observable, of } from 'rxjs';

import { IUser } from '../../interfaces/i-user';
import * as fakeData from '../../mock-data/clients.json';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService implements BaseApiServiceService {

  private static isMatching(value: string, searchStr: string): boolean {
    return value.toString().toLowerCase().indexOf(searchStr) > -1;
  }

  search(searchStr: string): Observable<IUser[]> {
    const users = fakeData.users.filter((u: IUser) => this.isUserMatchingSearchString(u, searchStr));
    return of(users).pipe(delay(1000));
  }

  private isUserMatchingSearchString(user: IUser, searchStr: string): boolean {
    if (!searchStr) {
      return true;
    }
    const lowerStr = searchStr.toLowerCase();
    return this.isObjectMatching(user, ['firstName', 'lastName', 'phoneNumber', 'description'], lowerStr);
  }

  private isObjectMatching(obj: any, fields: string[], searchStr: string): boolean {
    return fields.some(field => {
      const value = obj[field];
      if (value && ['string', 'number'].includes(typeof(value))) {
        return ApiService.isMatching(value, searchStr);
      }
      return false;
    });
  }
}
