import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { IUser } from '../../../interfaces/i-user';
import { ApiService } from '@core/services/api.service';
import { tap } from 'rxjs/operators';

@Injectable()
export class UsersService {

  private selectedUserId = new BehaviorSubject<string>('');
  selectedUser$ = this.selectedUserId.asObservable();
  private cachedUsers: IUser[];

  constructor(private apiService: ApiService) {
  }

  searchUsers(searchStr: string): Observable<IUser[]> {
    return this.apiService.search(searchStr).pipe(tap((users: IUser[]) => this.cachedUsers = users));
  }

  setSelectedUser(userId: string): void {
    this.selectedUserId.next(userId);
  }

  getUserById(userId: string): Observable<IUser> {
    return of(this.cachedUsers && this.cachedUsers.find((user: IUser) => user.id === userId));
  }
}
