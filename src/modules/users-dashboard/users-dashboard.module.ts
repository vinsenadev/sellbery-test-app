import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '@shared/shared.module';
import { BaseApiServiceService } from '@core/services/base-api-service.service';
import { ApiService } from '@core/services/api.service';
import { SharedMaterialModule } from '@shared-material/shared-material.module';
import { UsersService } from './services/users.service';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { UsersListComponent } from './components/users-list/users-list.component';

@NgModule({
  declarations: [DashboardComponent, UsersListComponent],
  imports: [CommonModule, SharedMaterialModule, ReactiveFormsModule, SharedModule],
  providers: [
    UsersService,
    {provide: BaseApiServiceService, useValue: ApiService}],
  exports: [DashboardComponent]
})
export class UsersDashboardModule {
}
