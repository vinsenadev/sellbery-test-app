import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  ViewChild
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, map, share, startWith, switchMap, tap } from 'rxjs/operators';

import { UsersService } from '@users-dashboard/services/users.service';
import { IUser } from '../../../../interfaces/i-user';

const SEARCH_DEBOUNCE_TIME = 1000;

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UsersListComponent implements OnInit, AfterViewInit {

  @ViewChild('searchInput') inputEl: ElementRef;

  searchInputControl = new FormControl();
  isLoading: boolean;
  users$: Observable<IUser[]>;
  selectedUserId$: Observable<string>;

  constructor(private usersService: UsersService, private cdRef: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.subscribeToSearchInput();

    this.subscribeOnSelectedUser();
  }

  ngAfterViewInit(): void {
    this.inputEl.nativeElement.focus();
  }

  onSelect(userId: string): void {
    this.usersService.setSelectedUser(userId);
  }

  private subscribeOnSelectedUser(): void {
    this.selectedUserId$ = this.usersService.selectedUser$;
  }

  private subscribeToSearchInput(): void {
    this.users$ = this.searchInputControl.valueChanges
      .pipe(
        startWith(''),
        tap(() => this.setLoadingState(true)),
        debounceTime(SEARCH_DEBOUNCE_TIME),
        map((searchStr: string) => searchStr && searchStr.trim()),
        distinctUntilChanged(),
        switchMap((searchStr: string) => this.usersService.searchUsers(searchStr)),
        tap(() => this.setLoadingState(false)),
        catchError((error) => {
          // TODO: implement display of notifications to the user
          console.error(error);
          this.setLoadingState(false);
          return of([]);
        }),
        share()
      ) as Observable<IUser[]>;
  }

  private setLoadingState(state: boolean): void {
    this.isLoading = state;
    this.cdRef.markForCheck();
  }
}
